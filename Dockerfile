FROM python:3
ADD . /code
WORKDIR /code
RUN pip install Flask
RUN pip install tensorflow
RUN pip install numpy
RUN pip install h5py
RUN pip install bson
RUN pip install requests
RUN pip install Pillow
RUN pip install keras_applications
RUN pip install pymongo
RUN pip install keras
CMD ["python", "main.py"]`